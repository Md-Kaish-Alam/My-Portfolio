# Personal Portfolio

Welcome to my personal portfolio, a comprehensive showcase of my skills, personal projects, and professional experiences. Designed with responsiveness in mind, this portfolio is built to provide a seamless experience across all devices.

**Try it out now: [link](https://md-kaish-alam-portfolio.netlify.app/)**

https://github.com/Md-Kaish-Alam/My-Portfolio/assets/82415398/88ba41b1-d4f2-4ce5-a153-014e18bc2552

## Tech Stack

### Vite
- **Description**: A build tool that aims for a faster development experience by providing lightning-fast build times and instant hot module replacement (HMR).
- **Use in Project**: Used for building and serving the application with optimized performance.
- **Documentation**: [Vite Documentation](https://vitejs.dev/guide/)

### ReactJS
- **Description**: A JavaScript library for building user interfaces, enabling the development of dynamic, reusable UI components.
- **Use in Project**: Powers the frontend, facilitating the creation of a rich, interactive user experience.
- **Documentation**: [React Documentation](https://reactjs.org/docs/getting-started.html)

### JavaScript (ES6+)
- **Description**: The latest version of JavaScript, offering new syntax and features for writing cleaner, more concise code.
- **Use in Project**: Used for scripting and implementing interactive features in the application.
- **Documentation**: [MDN Web Docs](https://developer.mozilla.org/en-US/docs/Web/JavaScript)

### Framer Motion
- **Description**: A library for React that makes it simple to create complex animations and transitions.
- **Use in Project**: Enhances UI with smooth animations, improving user engagement.
- **Documentation**: [Framer Motion Documentation](https://www.framer.com/docs/)

### Sass
- **Description**: A preprocessor scripting language that is interpreted or compiled into Cascading Style Sheets (CSS).
- **Use in Project**: Used for writing more maintainable, structured CSS.
- **Documentation**: [Sass Documentation](https://sass-lang.com/documentation)

### Sanity
- **Description**: A platform for structured content that comes with an open-source editing environment called Sanity Studio that you can customize with React.js.
- **Use in Project**: Acts as the backend, managing content for the portfolio.
- **Documentation**: [Sanity Documentation](https://www.sanity.io/docs)

### Netlify
- **Description**: A cloud service that automates the build and deployment of static websites, offering CI/CD, serverless functions, and more.
- **Use in Project**: Used for deploying and hosting the portfolio, ensuring high availability and performance.
- **Documentation**: [Netlify Documentation](https://docs.netlify.com/)

## Features

- **Responsive Design**: Ensures the portfolio is accessible and performs well across all devices.
- **Contact Form**: Simplifies communication by allowing visitors to contact me directly through the website.
- **Testimonials**: Builds credibility by showcasing feedback and testimonials from colleagues and clients.
- **Project Showcase**: Displays personal projects and professional experiences with engaging descriptions and visuals.
- **Sanity CMS Integration**: Facilitates easy content management and updates through Sanity's user-friendly backend.
- **Performance Optimized**: Leverages modern development tools and practices for fast loading times and smooth site performance.
- **Scalable Architecture**: Designed with scalability in mind, making it easy to add new features or content.

## Project Structure

```plaintext
/portfolio-project
│
├── public/                   # Public files like index.html
├── src/
│   ├── components/           # Reusable components
│   ├── pages/                # Page components
│   ├── styles/               # SASS files for styling
│   ├── utils/                # Utility functions
│   └── App.js                # Main React application file
│
├── sanity/                   # Sanity studio project
│
├── .gitignore                # Specifies intentionally untracked files to ignore
├── package.json              # Node module dependencies and scripts
├── README.md                 # Documentation of the project (this file)
└── vite.config.js            # Vite configuration
```

## Installation Steps

To get this portfolio running on your local machine, follow these steps:

1. **Clone the repository**

```bash
git clone https://github.com/Md-Kaish-Alam/My-Portfolio.git
cd personal-portfolio
```

2. **Install Dependencies**
   
```bash
npm install
```

3. **Run the development server**

```bash
npm run dev
```

The portfolio should now accessible at `http://localhost:3000`

**Note: For installation and set up Sanity prefer the sanity official documentation. [Docs](https://www.sanity.io/docs)**

## Getting in Touch
Feel free to reach out if you have any questions or just want to connect:
- Email: [alamkaishg1511@gmail.com](mailto:alamkaishg1511@gmail.com)
- Phone: +91-7061238198, +91-8235527277

**Thank you for checking out my portfolio**

---

**© 2023 MdKaishAlam. All Rights Reserved.**
